import os
import hashlib

def search_dir(directory, contents = [], filenames = []):
	""" Takes a directory path and recursively search all subdirectories.
	Stores the filenames and their paths and returns them.
	"""
	items = os.listdir(directory)
	for item in items:
		path = directory + "/" + item
		if os.path.isdir(path):
			search_dir(path, contents, filenames)
		else:
			contents.append(path)
			filenames.append(item)
	return contents, filenames


def log_file(file_path):
	""" Hashes a file and returns it """
	# Open the file as binary
	with open(file_path, 'rb') as f:
		hash_object = (hashlib.md5(f.read()))

	return hash_object.hexdigest()


def create_dict(paths_list, filenames):
	""" Loads two list into a dictionary. Retruns the dictionary """
	file_dict = {}
	count = 0
	for path in paths_list: # Creates a dictionary for filename and hash value
		name = filenames[count]
		hashed = log_file(path)
		file_dict[name] = hashed
		count += 1

	return file_dict



# Filehandeling Functions
def write_to_file(file_dict, path):
	""" Writes a dictionary into a file line by line. Returns nothing """
	# Combines the filename and hash values into tuples. This groups the content that will be stored together.  
	tuples = [(a,b) for a,b in zip(filenames, file_dict.values())] 

	with open(path, "w+") as f:
		for i in tuples:
			f.writelines(i[0] + ":" + i[1] + "\n")


def read_from_file(path):
	""" Reads the log_file and load it into a dictionary. Returns the dictionary. """
	new_dict = {}
	with open(path, "r") as f:
		data = f.readlines()

	for item in data:
		items = item.split(":")
		new_dict[items[0]] = items[1]
			
	return new_dict



# Comparison Functions
def check_for_file_birth(new_dict, original_dict):
	""" Takes the new dictionary and the original then it determents if a files been added.
	Returns a list of added files.  """
	difference = new_dict.keys() - original_dict.keys() # Search for filenames in new_dict that does not exists in original_dict.
	result = []
	if difference != 0:
		for i in difference:
			result.append(i)

	return result # List of keys (filenames) that has been removed.


def check_for_file_removal(new_dict, original_dict):
	""" Takes the new dictionary and the original then it determents if a files been removed.
	Returns a list of removed files.
	"""
	difference =  original_dict.keys() - new_dict.keys() #Search for filenames in original_dict that does not exists in new_dict.
	result = []
	if difference != 0:
		for i in difference:
			result.append(i)
			
	return result # List of keys (filenames) that has been added.


def check_for_file_changes(new_dict, original_dict):
	""" Takes the new dictionary and the original. Checks for changes in hash values. Returns a list of changed files """
	commonkeys = original_dict.keys() & new_dict.keys()
	result = []
	for key in commonkeys:
		if original_dict.get(key) != (new_dict.get(key) + "\n"): # "\n" must be added because we add it when we use write_to_file function.
			result.append(key)

	return result # List of keys (filenames) that has been changed.



# User-interface Functions
def display(added_files, removed_files, changed_files):
	""" Prints out all the added, removed and changed files that has been found in this run. Returns nothing"""
	
	# Prints the result of the added_file function.
	if added_files != []:
		for f in added_files:
			print(f"{f} has been added\n")
	else:
		print ("No files added\n")

	# Prints the result of the removed_file function.
	if removed_files != []:
		for f in removed_files:
			print(f"{f} has been removed\n")
	else:
		print ("No files removed\n")

	# Prints the result of the changed_files function.
	if changed_files != []:
		for f in changed_files:
			print(f"{f} has been changed\n")
	else:
		print ("No files changes\n")


def get_user_input():
	""" Request a user to input a path to a directory . Returns the path if it exist otherwise request new input"""
	test = True
	while test is True:
		root_path = input("Enter a path to directory: ")

		if os.path.exists(root_path):
			test = False
			return root_path
		print("Not a valid path\n")


 
# Creates the file where the program is located.
path_of_log_file = "log_file.txt"

chosen_dir = get_user_input()
paths_list, filenames = search_dir(chosen_dir)

# New dictionary is created from the search_dir() and is now the current dictionary.
new_dict = create_dict(paths_list, filenames)


if os.path.exists(path_of_log_file):
	#The log_file already exist so it have to be the original.
	original_dict = read_from_file(path_of_log_file)

	added_files = check_for_file_birth(new_dict, original_dict)
	removed_files = check_for_file_removal(new_dict, original_dict)
	changed_files = check_for_file_changes(new_dict, original_dict)

	display(added_files, removed_files, changed_files)

	#Writes over the log_file with the new_dict so we can se the new change after the check.
	write_to_file(new_dict, path_of_log_file)

else:
	#If a log_file does not exist we make one.
	write_to_file(new_dict, path_of_log_file)
	print("- Log File has been created - \n")